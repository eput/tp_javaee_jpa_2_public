package utils;

import models.Mepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is automatically loaded when your application is started (@Startup)
 * Additionally, we tell the Application Server that we want at most one instance of this class (@Singleton)
 */
@Startup
@Singleton
public class ApplicationData {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationData.class);
    @Inject
    private EntityManager em;

    @PostConstruct
    public void initData(){
        List<Mepo> mepi = new ArrayList<Mepo>();
        mepi.add(new Mepo("John Smice", "This is my first Mepo, awesome isn't it ?"));
        mepi.add(new Mepo("Poca Hontas", "Where are you John ?"));
        for (Mepo mepo : mepi) {
            em.merge(mepo);
        }
        LOGGER.info("Mepi repository initialized");
    }
}
