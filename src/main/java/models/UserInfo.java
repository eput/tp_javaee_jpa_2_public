package models;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;


@Named
@SessionScoped
public class UserInfo implements Serializable {
    private String nickName;
    private String password;
    private Boolean loggedIn;

    public UserInfo() {
        this.loggedIn = Boolean.FALSE;
    }

    /************************** GETTER / SETTERS ****************************/
    public String getNickName() {
        return nickName;
    }

    public void setNickName(final String nickName) {
        this.nickName = nickName;
    }

    public Boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(final Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
